# disable all default rules
.SUFFIXES:

# some of these should be set by the user before building
POLICY_NAME ?= bauen1-policy
POLICY_VERSION ?= 32

#
DESTDIR ?= /
SYSCONFDIR ?= /etc
SHAREDSTATEDIR ?= /var/lib
SECILC ?= secilc
SEMODULE ?= semodule
SEMODULE_CORE_PRIORITY ?= 100
SEMODULE_OPTIONAL_PRIORITY ?= 200
SETFILES ?= /usr/sbin/setfiles
PANDOC ?= pandoc

SECILC_OPTIONS ?= -vvv

MODULES_BASE ?= $(sort $(shell find policy/base -name "*.cil"))
MODULES_CONSTRAINTS ?= $(sort $(shell find policy/constraints -name "*.cil"))
MODULES_KERNEL ?= $(sort $(shell find policy/kernel -name "*.cil"))
MODULES_MISC ?= $(sort $(shell find policy/misc -name "*.cil"))
MODULES_ROLES ?= $(sort $(shell find policy/roles -name "*.cil"))
MODULES_SERVICES ?= $(sort $(shell find policy/services -name "*.cil"))
MODULES_SYSTEM ?= $(sort $(shell find policy/system -name "*.cil"))

MODULES_CORE := $(MODULES_BASE) $(MODULES_CONSTRAINTS) $(MODULES_KERNEL) $(MODULES_ROLES) $(MODULES_SYSTEM)
MODULES_OPTIONAL := $(MODULES_MISC) $(MODULES_SERVICES)

MODULES_ALL := $(MODULES_CORE) $(MODULES_OPTIONAL)

# Helper variables for shorter code
SELINUX_CONFDIR ?= $(SYSCONFDIR)/selinux/$(POLICY_NAME)

# user targets
.PHONY: all
all: policy-all.$(POLICY_VERSION)

.PHONY: clean
clean:
	rm -f *.html docs/*.html
	rm -f policy-core.$(POLICY_VERSION) file_contexts_generated-core.$(POLICY_VERSION)
	rm -f policy-all.$(POLICY_VERSION) file_contexts_generated-all.$(POLICY_VERSION)

.PHONY: docs
docs: README.html docs/IPsec.html docs/sepgsql.html

.PHONY: install
install: install-config install-semodules install-misc

.PHONY: install-config
install-config: \
	file_contexts_generated-all.$(POLICY_VERSION) config/dbus_contexts config/failsafe_context \
	config/default_contexts config/default_type config/users/sysadm.user config/users/user.user config/users/lightdm.user \
	config/file_contexts.subs_dist config/setrans.conf config/sepgsql_contexts config/customizable_types config/virtual_domain_context config/virtual_image_context
	install --directory $(DESTDIR)$(SELINUX_CONFDIR)/contexts/files
	install --mode 0644 file_contexts_generated-all.$(POLICY_VERSION) $(DESTDIR)$(SELINUX_CONFDIR)/contexts/files/file_contexts
	install --directory $(DESTDIR)$(SELINUX_CONFDIR)/contexts/users
	install --mode 0644 config/dbus_contexts $(DESTDIR)$(SELINUX_CONFDIR)/contexts/dbus_contexts
	install --mode 0644 config/failsafe_context $(DESTDIR)$(SELINUX_CONFDIR)/contexts/failsafe_context
	install --mode 0644 config/default_contexts $(DESTDIR)$(SELINUX_CONFDIR)/contexts/default_contexts
	install --mode 0644 config/default_type $(DESTDIR)$(SELINUX_CONFDIR)/contexts/default_type
	install --mode 0644 config/users/sysadm.user $(DESTDIR)$(SELINUX_CONFDIR)/contexts/users/sysadm.user
	install --mode 0644 config/users/user.user $(DESTDIR)$(SELINUX_CONFDIR)/contexts/users/user.user
	install --mode 0644 config/users/lightdm.user $(DESTDIR)$(SELINUX_CONFDIR)/contexts/users/lightdm.user
	install --mode 0644 config/file_contexts.subs_dist $(DESTDIR)$(SELINUX_CONFDIR)/contexts/files/file_contexts.subs_dist
	install --mode 0644 config/setrans.conf $(DESTDIR)$(SELINUX_CONFDIR)/setrans.conf
	install --mode 0644 config/sepgsql_contexts $(DESTDIR)$(SELINUX_CONFDIR)/contexts/sepgsql_contexts
	install --mode 0644 config/customizable_types $(DESTDIR)$(SELINUX_CONFDIR)/contexts/customizable_types
	install --mode 0644 config/virtual_domain_context $(DESTDIR)$(SELINUX_CONFDIR)/contexts/virtual_domain_context
	install --mode 0644 config/virtual_image_context $(DESTDIR)$(SELINUX_CONFDIR)/contexts/virtual_image_context

.PHONY: install-semodules
install-semodules: $(MODULES_ALL)
	install --directory $(DESTDIR)$(SHAREDSTATEDIR)/selinux/$(POLICY_NAME)
	$(SEMODULE) \
		--priority $(SEMODULE_CORE_PRIORITY) \
		--noreload \
		--store "$(POLICY_NAME)" \
		--path "$(DESTDIR)" \
		$(addprefix --install=,$(MODULES_CORE)) \
		--priority $(SEMODULE_OPTIONAL_PRIORITY) \
		$(addprefix --install=,$(MODULES_OPTIONAL))

.PHONY: install-misc
install-misc: secil.nanorc
	install --directory $(DESTDIR)/usr/share/nano
	install --mode 0644 secil.nanorc $(DESTDIR)/usr/share/nano/secil.nanorc

.PHONY: validate
validate: validate-core validate-all

.PHONY: validate-%
validate-%: policy-%.$(POLICY_VERSION) validate-filecontexts-%
	@

.PHONY: validate-filecontexts-%
validate-filecontexts-%: policy-%.$(POLICY_VERSION) file_contexts_generated-%.$(POLICY_VERSION)
	$(SETFILES) -c "policy-$*.$(POLICY_VERSION)" "file_contexts_generated-$*.$(POLICY_VERSION)"

# build targets
policy-core.% file_contexts_generated-core.%: $(MODULES_CORE)
	$(SECILC) $(SECILC_OPTIONS) -vvv --policyvers=$* --output="policy-core.$*" --filecontext="file_contexts_generated-core.$*" $^

policy-all.% file_contexts_generated-all.%: $(MODULES_ALL)
	$(SECILC) $(SECILC_OPTIONS) -vvv --policyvers=$* --output="policy-all.$*" --filecontext="file_contexts_generated-all.$*" $^

%.html: %.md
	$(PANDOC) --output $@ --standalone --metadata title="$*" $<
