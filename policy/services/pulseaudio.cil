;;
;; pulseaudio
;;

(block pulseaudio
    ;;
    ;; Configuration Files
    ;;
    (block conf
        ;;
        ;; Declarations
        ;;

        (blockinherit .file.conf.template)
        (blockinherit .file.template_simple_file)

        ;;
        ;; Contexts
        ;;

        (filecon "/etc/pulse(/.*)?" dir context_lowhigh)
        (filecon "/etc/pulse/.*" file context_lowhigh)
        (filecon "/etc/pulse/.*" symlink context_lowhigh)
    )

    ;;
    ;; User Runtime Files
    ;;
    (block user_runtime
        ;;
        ;; Declarations
        ;;

        (blockinherit .user.runtime.template)
        (blockinherit .file.template_dir)
        (blockinherit .file.template_file)
        (blockinherit .file.template_sock_file)

        (roletype daemon.roles type)

        ;;
        ;; Contexts
        ;;

        (filecon "/run/user/%{USERID}/pulse" dir context_lowhigh)
        (filecon "/run/user/%{USERID}/pulse/native" socket context_lowhigh)
        (filecon "/run/user/%{USERID}/pulse/pid" file context_lowhigh)
    )

    ;;
    ;; User Configuration Files
    ;;
    (block home_config
        ;;
        ;; Declarations
        ;;

        (blockinherit .user.home.config.template)
        (blockinherit .file.template_dir)
        (blockinherit .file.template_file)

        ;;
        ;; Contexts
        ;;

        (filecon "HOME_DIR/\.config/pulse" dir context_lowhigh)
        (filecon "HOME_DIR/\.config/pulse/.*" file context_lowhigh)

        ;;
        ;; Policy
        ;;

        (roletype daemon.roles type)

        ;;
        ;; Macros
        ;;

        (macro admin ((role role)(type domain))
            (call admin_dirs (role domain))
            (call admin_files (role domain))
        )
    )

    ;;
    ;; Memory Files
    ;;
    (block mem
        ;;
        ;; Declarations
        ;;

        (blockinherit .fs.tmp.template)
        (blockinherit .file.template_file)

        ;;
        ;; Policy
        ;;

        (roletype daemon.roles type)
    )

    (block daemon
        ;;
        ;; Declarations
        ;;

        (blockinherit .application.template)

        ;;
        ;; Contexts
        ;;

        (filecon "/usr/bin/pulseaudio" file exec.context_lowhigh)

        ;;
        ;; Policy
        ;;

        (allow type self .process.getsched)
        (allow type self .process.setcap)
        (allow type self .process.setrlimit)
        (allow type self .process.setsched)
        (allow type self .process.signal)
        (allow type self .netlink_kobject_uevent_socket.create)
        (allow type self .netlink_kobject_uevent_socket.bind)
        (allow type self .unix_stream_socket.shutdown)

        (call rw_inherited_pipes (type))

        (call exec.execute_files (type))

        (call .systemd.user.daemon_type (type exec.type))
        (call .systemd.user.named_socket_activation (type user_runtime.type))
        (call .systemd.user.notify_startup (type))
        (call .systemd.logind.client.session.domain_type (type))

        (call conf.read_simple_files (type))

        (call user_runtime.manage_dirs (type))
        (call user_runtime.manage_files (type))
        (call user_runtime.map_exec_files (type))
        (call user_runtime.manage_sock_files (type))
        (call .user.runtime.type_transition (type dir "pulse" user_runtime.type))
        (call .user.runtime.type_transition (type file "*" user_runtime.type))

        (call home_config.manage_dirs (type))
        (call home_config.manage_files (type))
        (call .user.home.config.type_transition (type dir "pulse" home_config.type))
        (call .user.home.config.create_dirs (type))
        (call .user.home.config.automatic_type_transition (type))

        (call mem.manage_files (type))
        (call .fs.tmp.type_transition (type file "*" mem.type))

        (call .seutil.libselinux_linked (type))

        (call .dev.random.read_chr_files (type))
        ; /dev/snd
        (call .dev.list_dirs (type))
        (call .dev.watch_dirs (type))
        (call .dev.snd.map_rw_chr_files (type))
        (call .dev.snd.ioctl_chr_files (type))
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_PVERSION)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_INFO)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_TTSTAMP)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_USER_PVERSION)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_HW_REFINE)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_HW_PARAMS)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_HW_FREE)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_SW_PARAMS)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_HWSYNC)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_STATUS_EXT)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_CHANNEL_INFO)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_PREPARE)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_START)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_DROP)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_PCM_IOCTL_REWIND)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_CTL_IOCTL_PVERSION)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_CTL_IOCTL_CARD_INFO)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_CTL_IOCTL_ELEM_LIST)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_CTL_IOCTL_ELEM_INFO)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_CTL_IOCTL_ELEM_READ)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_CTL_IOCTL_ELEM_WRITE)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_CTL_IOCTL_ELEM_LOCK)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_CTL_IOCTL_ELEM_UNLOCK)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_CTL_IOCTL_SUBSCRIBE_EVENTS)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_CTL_IOCTL_ELEM_ADD)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_CTL_IOCTL_TLV_READ)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_CTL_IOCTL_TLV_WRITE)
        (allowx type .dev.snd.type .chr_file.ioctl.SNDRV_CTL_IOCTL_PCM_PREFER_SUBDEVICE)

        (call .kernel.proc.asound.read_simple_files (type))

        (call .sys.getattr_fs (type))
        (call .sys.devices.read_simple_files (type))
        (call .sys.devices.cpu.list_dirs (type))
        (call .sys.devices.cpu.online.read_files (type))

        (call .udev.runtime.search_dirs (type))
        (call .udev.runtime.read_files (type))

        (call .file.misc.locale.read_locales (type))
        (call .file.usr.read_simple_files (type))

        (call .logging.send_syslog_messages (type))

        (call .dbus.client.domain_type (type))
        (call .dbus.session.client.domain_type (type))
        (call .dbus.session.client.acquire_service (type))

        (call .hostname.misc.read_files (type))

        (optional opt_alsa
            (call .alsa.conf.read_simple_files (type))
        )

        (optional opt_rtkit
            (call .rtkit.dbus_chat (type))
        )

        (optional opt_polkit
            (call .polkit.client.domain_type (type))
        )

        (optional opt_bluetoothd
            (call .bluetoothd.dbus_chat (type))

            ; audio headset
            (allow type .bluetoothd.type .bluetooth_socket.rw_inherited)
            (allow type .bluetoothd.type .bluetooth_socket.shutdown)
            (allow type .bluetoothd.type .bluetooth_socket.getopt)
            (allow type .bluetoothd.type .bluetooth_socket.setopt)
            (allow type .bluetoothd.type .fd.use)
        )

        (optional opt_xserver
            ; module-x11-xsmp module
            (allow type .xserver.type .unix_stream_socket.connectto)
            (call .xserver.xauth.read_files (type))
            (call read_state (.xserver.type))
            (call .xserver.xserver_iceauth_client (type))
        )
    )

    ;;
    ;; Macros
    ;;

    (macro admin ((role role)(type domain))
        (call conf.admin_simple_files (role domain))
    )

    (macro stream_connect ((type domain))
        (allow domain self .unix_stream_socket.create)
        (allow domain self .unix_stream_socket.connect)
        (call user_runtime.search_dirs (domain))
        (call user_runtime.write_sock_files (domain))
        (allow domain daemon.type .unix_stream_socket.connectto)
    )

    ;;
    ;; Clients
    ;;

    (block client
        ;;
        ;; Declarations
        ;;

        (blockinherit .utils.generic.template)

        (block mem
            (blockinherit .utils.generic.template)

            (allow daemon.type types .file.map_rw_inherited)
        )

        ;;
        ;; Policy
        ;;

        (call stream_connect (types))
        (call user_runtime.list_dirs (types))

        (call home_config.list_dirs (types))
        (call home_config.read_files (types))
        (call home_config.lock_files (types))

        (call conf.read_simple_files (types))

        (allow types daemon.type .fd.use)
        (allow daemon.type types .fd.use)

        (allow types .pulseaudio.mem.type .file.map_rw_inherited)

        (optional opt_alsa
            (call .alsa.conf.read_simple_files (types))
        )
    )
)
