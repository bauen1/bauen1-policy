;;
;; virt: libvirtd
;;

(block virt
    ;;
    ;; Configuration Files
    ;;
    (block conf
        ;;
        ;; Declarations
        ;;

        (blockinherit .file.conf.template)
        (blockinherit .file.conf.template_generic)
        (call specific_type (type))
        (blockinherit .file.template_simple_file)

        ;;
        ;; Contexts
        ;;

        (filecon "/etc/libvirt" dir context_lowhigh)
        (filecon "/etc/libvirt/.*" file context_lowhigh)

        ;;
        ;; Policy
        ;;

        (call .init.daemon_env_file.specific_type (types))
        (call .init.daemon_conf.specific_type (types))

        ;;
        ;; Templates
        ;;

        (block template
            (blockabstract template)
            (blockinherit .file.conf.template)
            (call .virt.conf.specific_type (type))
            (call .virt.conf.search_dirs (parent_dir_search_types))
        )
    )

    ;;
    ;; Runtime Files
    ;;
    (block runtime
        ;;
        ;; Declarations
        ;;

        (blockinherit .file.runtime.template)
        (blockinherit .file.runtime.template_generic)
        (call specific_type (type))
        (blockinherit .file.template_dir)
        (blockinherit .file.template_file)

        ;;
        ;; Contexts
        ;;

        (filecon "/run/libvirt" dir context_lowhigh)
        (filecon "/run/libvirt/.*" any ())
        (filecon "/run/libvirt/nwfilter(/.*)" any context_lowhigh)

        ;;
        ;; Policy
        ;;

        (call .init.daemon_runtime.specific_type (types))

        ;;
        ;; Macros
        ;;

        (macro automatic_type_transition ((type domain))
            (call .file.runtime.type_transition (domain dir "libvirt" type))
        )

        ;;
        ;; Templates
        ;;

        (block template
            (blockabstract template)
            (blockinherit .file.runtime.template)
            (call .virt.runtime.specific_type (type))
            (call .virt.runtime.search_dirs (parent_dir_search_types))
        )
    )

    ;;
    ;; State Files
    ;;
    (block state
        ;;
        ;; Declarations
        ;;

        (blockinherit .file.var.lib.template)
        (blockinherit .file.var.lib.template_generic)
        (call specific_type (type))
        (blockinherit .file.template_dir)

        ;;
        ;; Contexts
        ;;

        (filecon "/var/lib/libvirt" dir context_lowhigh)
        (filecon "/var/lib/libvirt/.*" any ())

        ;;
        ;; Policy
        ;;

        (call .init.daemon_state_type (types))

        ;;
        ;; Templates
        ;;

        (block template
            (blockabstract template)
            (blockinherit .file.var.lib.template)
            (call .virt.state.specific_type (type))
            (call .virt.state.search_dirs (parent_dir_search_types))
        )
    )

    ;;
    ;; Log Files
    ;;
    (block log
        (blockinherit .file.var.log.template)
        (blockinherit .file.template_dir)
        (blockinherit .file.template_file)

        (filecon "/var/log/libvirt(/.*)?" any context_lowhigh)

        (optional opt_logrotate
            (call .logrotate.client.log.obj_type (type))
        )
    )

    ;;
    ;; Cache Files
    ;;
    (block cache
        (blockinherit .file.var.cache.template)
        (blockinherit .file.template_dir)
        (blockinherit .file.template_file)

        (filecon "/var/cache/libvirt(/.*)?" any context_lowhigh)
    )

    (block daemon
        ;;
        ;; Declarations
        ;;

        (blockinherit .domain.template)
        (blockinherit .system.template_daemon_domain)

        (block conf
            (blockinherit .virt.conf.template)
            (blockinherit .file.template_simple_file)
        )

        (block runtime
            (blockinherit .virt.runtime.template)
            (blockinherit .file.template_dir)
            (blockinherit .file.template_file)
            (blockinherit .file.template_sock_file)

            (call .constraints.rbacsep.exemption_object_type (type))

            ;;
            ;; Contexts
            ;;

            (filecon "/run/libvirtd\.pid" file context_lowhigh)
            (filecon "/run/libvirt/libvirt-admin-sock" socket context_lowhigh)
            (filecon "/run/libvirt/libvirt-sock" socket context_lowhigh)
            (filecon "/run/libvirt/libvirt-sock-ro" socket context_lowhigh)
        )

        (block lock
            (blockinherit .file.runtime.lock.template)
            (blockinherit .file.template_file)
        )

        (block unit
            (blockinherit .init.unit.template)
        )

        ;;
        ;; Contexts
        ;;

        (filecon "/usr/sbin/libvirtd" file exec.context_lowhigh)

        (filecon "/etc/default/libvirtd" file conf.context_lowhigh)
        (filecon "/etc/libvirt/libvirt\.conf" file conf.context_lowhigh)
        (filecon "/etc/libvirt/libvirtd\.conf" file conf.context_lowhigh)
        (filecon "/etc/libvirt/qemu(/.*)?" any conf.context_lowhigh)
        (filecon "/etc/libvirt/qemu\.conf" any conf.context_lowhigh)
        (filecon "/etc/libvirt/secrets(/.*)?" any conf.context_lowhigh)
        (filecon "/etc/libvirt/storage(/.*)?" any conf.context_lowhigh)
        (filecon "/etc/libvirt/nwfilter(/.*)?" any conf.context_lowhigh)

        (filecon "/usr/lib/systemd/system/libvirtd-admin\..*" any unit.context_lowhigh)
        (filecon "/usr/lib/systemd/system/libvirtd-ro\..*" any unit.context_lowhigh)
        (filecon "/usr/lib/systemd/system/libvirtd-tcp\..*" any unit.context_lowhigh)
        (filecon "/usr/lib/systemd/system/libvirtd-ssl\..*" any unit.context_lowhigh)
        (filecon "/usr/lib/systemd/system/libvirtd\..*" any unit.context_lowhigh)

        ;;
        ;; Policy
        ;;

        (allow type self (capability (kill setgid setuid chown fsetid fowner dac_override dac_read_search net_admin sys_admin sys_nice sys_ptrace sys_chroot ipc_lock mknod)))
        (allow type self (capability2 (bpf perfmon)))
        (allow type self (process (getcap setcap signal signull sigkill getsched setsched execmem setsockcreate setrlimit)))
        (allow type self .netlink_route_socket.create) ; FIXME: probably part of a macro
        (allow type self .netlink_route_socket.bind)
        (allow type self .netlink_route_socket.nlmsg_rw)
        (allow type self .unix_stream_socket.bind)
        (allow type self .unix_stream_socket.listen)
        (allow type self .unix_stream_socket.accept)
        (allow type self .unix_stream_socket.connectto)
        (allow type self .udp_socket.create) ; FIXME: proabbyl part of a macro
        (allow type self .udp_socket.connect)
        (allow type self .netlink_kobject_uevent_socket.create)
        (allow type self .netlink_kobject_uevent_socket.bind)
        (allow type self .netlink_generic_socket.create)
        (allow type self .netlink_generic_socket.bind)
        (allow type self .tun_socket.create)
        (allow type self .tun_socket.relabel)
        (allow type self .unix_dgram_socket.ioctl)
        (allowx type self .unix_dgram_socket.ioctl.SIOCGIFINDEX)
        (allowx type self .unix_dgram_socket.ioctl.SIOCGIFFLAGS)
        (allowx type self .unix_dgram_socket.ioctl.SIOCSIFFLAGS)
        (allowx type self .unix_dgram_socket.ioctl.SIOCETHTOOL)
        (allowx type self .unix_dgram_socket.ioctl.SIOCBRADDBR)
        (allowx type self .unix_dgram_socket.ioctl.SIOCBRADDIF)
        (allowx type self .unix_dgram_socket.ioctl.SIOCGIFMTU)
        (allowx type self .unix_dgram_socket.ioctl.SIOCSIFMTU)
        (allowx type self .unix_dgram_socket.ioctl.SIOCGIFHWADDR)
        (allowx type self .unix_dgram_socket.ioctl.SIOCSIFHWADDR)
        (allowx type self .unix_dgram_socket.ioctl.SIOCDEVPRIVATE)
        (allow type self .bpf.map_create)
        (allow type self .bpf.map_read)
        (allow type self .bpf.map_write)
        (allow type self .bpf.prog_load)
        (allow type self .bpf.prog_run)

        (call rw_inherited_pipes (type))

        (call .constraints.rbacsep.read_state_unconditional_exemption_source_type (type))
        (call .constraints.ibac.object_change_unconditional_exemption_source_type (type)) ; FIXME: due to bad label of /var/lib/libvirt/boot
        (call .constraints.rbac.object_change_unconditional_exemption_source_type (type)) ; FIXME: due to bad label of /var/lib/libvirt/boot

        (call .system.ranged_domain_type (type exec.type lowhigh))

        (call .init.named_socket_activation (type runtime.type))
        (call .systemd.notify_startup (type))

        ; /run/libvirt
        (call .virt.runtime.manage_dirs (type)) ; FIXME:
        (call .virt.runtime.manage_files (type)) ; FIXME:
        (call .virt.runtime.automatic_type_transition (type))
        ; /run/libvirt/qemu
        ; qemu-system is file.exec.type
        (call .svirt.domain_entry_file (.file.exec.type))
        (call .svirt.specified_transition_pattern (type .file.exec.type))
        (allow type .svirt.type .unix_stream_socket.connectto)
        (call .svirt.read_state (type))
        (call .virt.logd.use_fd (.svirt.type))
        (call .virt.logd.rw_inherited_pipes (.svirt.type))
        (call .svirt.pty.term_use_priv (type))
        (call .svirt.signull (type))
        (call .svirt.setsched (type))
        (call .svirt.runtime.manage_dirs (type))
        (call .svirt.runtime.mounton_dirs (type))
        (call .svirt.runtime.manage_files (type))
        (call .svirt.runtime.range_transition (type dir lowlow))
        (call .svirt.runtime.range_transition (type file lowlow))
        (call .virt.runtime.type_transition (type dir "qemu" .svirt.runtime.type))
        ; /run/libvirt/qemu/<guest>/dev is a tmpfs that (sadly) isn't relabeled properly
        (call .fs.tmp.manage_mount (type))
        (call .fs.tmp.manage_dirs (type))
        (call .fs.tmp.mounton_dirs (type))
        (call .fs.tmp.create_chr_files (type))
        (call .fs.tmp.setattr_chr_files (type))
        (call .fs.tmp.relabelfrom_chr_files (type))
        (call .fs.hugetlb.unmount (type))
        (call .fs.devpts.list_dirs (type))
        (call .fs.devpts.unmount (type))
        (call .fs.cgroup.manage_dirs (type))
        (call .fs.cgroup.rw_files (type))
        (call .fs.cgroup.read_lnk_files (type))
        (call .fs.ns.read_files (type))
        (call .dev.mounton_dirs (type))
        (call .dev.null.relabelto_chr_files (type))
        (call .dev.null.delete_chr_files (type))
        (call .dev.zero.relabelto_chr_files (type))
        (call .dev.zero.delete_chr_files (type))
        (call .dev.random.relabelto_chr_files (type))
        (call .dev.random.delete_chr_files (type))
        (call .dev.ptmx.relabelto_chr_files (type))
        (call .dev.ptmx.delete_chr_files (type))
        (call .dev.kvm.relabelto_chr_files (type))
        (call .dev.kvm.delete_chr_files (type))
        ; /run/libvirtd.pid
        (call runtime.manage_files (type))
        (call .file.runtime.type_transition (type file "libvirtd.pid" runtime.type))

        ; /run/lock/
        (call lock.manage_files (type))
        (call .file.runtime.lock.type_transition (type file "*" lock.type))

        ; write access to create /etc/libvirt/qemu/networks/default.xml.new on startup
        (call conf.manage_simple_files (type))
        ; write access to create /etc/libvirt/secrets, etc..
        (call .virt.conf.manage_simple_files (type))

        (call .svirt.state.manage_simple_files (type))
        (call .svirt.state.relabel_simple_files (type))
        (call .svirt.state.manage_sock_files (type))
        (call .svirt.image.manage_simple_files (type))
        (call .svirt.image.relabel_simple_files (type))
        (call .svirt.image.manage_sock_files (type))

        ; FIXME:
        (allow type .svirt.type .unix_stream_socket.create)
        (allow type .svirt.type .unix_stream_socket.bind)
        (allow type .svirt.type .unix_stream_socket.listen)

        (call cache.manage_dirs (type))
        (call cache.manage_files (type))

        ; FIXME:
        (call log.search_dirs (type))

        (call logd.client (type))

        (call .seutil.setfscreate (type))
        (call .seutil.translate_context (type))

        (call .nss.client.domain_type (type))

        (call .dbus.client.domain_type (type))

        (call .systemd.machined.dbus_chat (type))

        (call .logging.send_audit_messages (type))
        (call .logging.send_syslog_messages (type))

        (call .pam.read_active_users (type))

        (call .lvm.domtrans (type))
        (call .lvm.client.domain_type (type))

        (call .udev.runtime.search_dirs (type))
        (call .udev.runtime.read_files (type))

        (call .sys.getattr_fs (type))
        (call .sys.devices.read_simple_files (type))
        (call .sys.devices.write_files (type))
        (call .sys.devices.cpu.read_simple_files (type))
        (call .sys.devices.cpu.online.read_files (type))
        (call .sys.kernel.list_dirs (type))
        (call .sys.kernel.read_files (type))
        ; e.g. /sys/module/kvm_intel/parameters/nested
        (call .sys.module.search_dirs (type))
        (call .sys.module.read_files (type))
        ; dmidecode
        (call .sys.firmware.search_dirs (type))
        (call .sys.firmware.read_files (type))

        (call .dev.storage.fixed.read_blk_files (type))
        (call .dev.tun_tap.rw_chr_files (type))
        (call .dev.tun_tap.ioctl_chr_files (type))
        (allowx type .dev.tun_tap.type .chr_file.ioctl.TUNSETIFF)
        (allowx type .dev.tun_tap.type .chr_file.ioctl.TUNSETPERSIST)
        (allowx type .dev.tun_tap.type .chr_file.ioctl.TUNGETFEATURES)
        (allowx type .dev.tun_tap.type .chr_file.ioctl.TUNGETIFF)
        (call .dev.kvm.rw_chr_files (type))
        (call .dev.kvm.ioctl_chr_files (type))
        (allowx type .dev.kvm.type .chr_file.ioctl.KVM_GET_API_VERSION)
        (allowx type .dev.kvm.type .chr_file.ioctl.KVM_CREATE_VM)
        (allowx type .dev.kvm.type .chr_file.ioctl.KVM_GET_MSR_INDEX_LIST)
        (allowx type .dev.kvm.type .chr_file.ioctl.KVM_CHECK_EXTENSION)
        (allowx type .dev.kvm.type .chr_file.ioctl.KVM_GET_SUPPORTED_CPUID)
        (allowx type .dev.kvm.type .chr_file.ioctl.KVM_GET_MSR_FEATURE_INDEX_LIST)
        (allowx type .dev.kvm.type .chr_file.ioctl.KVM_GET_MSRS)
        (allowx type .dev.kvm.type .chr_file.ioctl.KVM_X86_GET_MCE_CAP_SUPPORTED)
        (allowx type .dev.kvm.type .chr_file.ioctl.KVM_GET_DEVICE_ATTR)
        (call .dev.vhost.rw_chr_files (type))
        (call .dev.random.read_chr_files (type))

        (call .fs.xattr.getattr_fs (type))
        (call .fs.hugetlb.getattr_fs (type))
        (call .fs.hugetlb.manage_dirs (type))

        (call .kernel.request_modules (type))
        (call .kernel.load_modules (type))
        (call .kernel.proc.miscinfo.read_files (type))
        (call .kernel.proc.systeminfo.read_files (type))
        (call .kernel.proc.mtrr.rw_files (type))
        (call .kernel.proc.read_files (type))
        ; /proc/sys/crypto/fips_enabled
        (call .kernel.proc.sysctl.crypto.search_dirs (type))
        (call .kernel.proc.sysctl.crypto.read_files (type))
        ; /proc/sys/kernel/cap_last_cap
        (call .kernel.sysctl.search_dirs (type))
        (call .kernel.sysctl.read_files (type))
        ;
        (call .net.sysctl.search_dirs (type))
        (call .net.sysctl.rw_files (type))

        (call .file.misc.locale.read_locales (type))
        (call .file.usr.read_simple_files (type))
        (call .file.exec.execute_files (type))
        (call .file.root.mounton_dirs (type))
        ; FIXME:
        (auditallow type .file.exec.type (file (execute_no_trans)))

        (call .nftables.domtrans (type))
        (call .udev.adm.run (.system.role type))

        (call .systemd.logind.client.inhibit.domain_type (type))

        ; FIXME:
        (call .file.cert.search_dirs (type))

        ; Strictly speaking dnsmasq is required by libvirtd
        (optional opt_dnsmasq
            (call .dnsmasq.domtrans (type))
            (call .dnsmasq.signal (type))
            (call .dnsmasq.signull (type))
            (call .dnsmasq.state.manage_dirs (type))
            (call .dnsmasq.state.manage_files (type))
            ; /run/libvirt/dnsmasq
            (call .dnsmasq.runtime.manage_dirs (type))
            (call .dnsmasq.runtime.manage_files (type))
            (call .virt.runtime.type_transition (type dir "network" .dnsmasq.runtime.type))
        )

        ;;
        ;; Macros
        ;;

        (macro stream_connect ((type domain))
            (allow domain self .unix_stream_socket.create)
            (allow domain self .unix_stream_socket.connect)
            (call .virt.runtime.search_dirs (domain))
            (call runtime.write_sock_files (domain))
            (allow domain type .unix_stream_socket.connectto)
        )

        (macro admin ((role role)(type domain))
            (call conf.admin_simple_files (role domain))

            ; virsh
            (call stream_connect (domain))
            (call .domain.read_state_pattern (daemon.type domain))
        )
    )

    (block lockd
        ;;
        ;; Declarations
        ;;

        (blockinherit .domain.template)
        (blockinherit .system.template_daemon_domain)

        (block conf
            (blockinherit .virt.conf.template)
            (blockinherit .file.template_file)
        )

        (block runtime
            (blockinherit .virt.runtime.template)
            (blockinherit .file.template_file)
            (blockinherit .file.template_sock_file)

            (filecon "/run/libvirt/virtlockd-admin-sock" socket context_lowhigh)
            (filecon "/run/libvirt/virtlockd-sock" socket context_lowhigh)
            (filecon "/run/virtlockd\.pid" file context_lowhigh)
        )

        (block unit
            (blockinherit .init.unit.template)
        )

        ;;
        ;; Contexts
        ;;

        (filecon "/usr/sbin/virtlockd" file exec.context_lowhigh)

        (filecon "/etc/default/virtlockd" file conf.context_lowhigh)
        (filecon "/etc/libvirt/virtlockd\.conf" file conf.context_lowhigh)

        (filecon "/usr/lib/systemd/virtlockd-admin\..*" any unit.context_lowhigh)
        (filecon "/usr/lib/systemd/virtlockd\..*" any unit.context_lowhigh)

        ;;
        ;; Policy
        ;;

        (allow type self (process (getsched)))
        (allow type self .unix_stream_socket.rw_inherited)
        (allow type self .unix_stream_socket.listen)
        (allow type self .unix_stream_socket.accept)

        (call rw_inherited_pipes (type))

        (call .virt.conf.read_files (type))
        (call conf.read_files (type))

        (call runtime.manage_sock_files (type))
        (call runtime.manage_files (type))
        (call .file.runtime.type_transition (type file "virtlockd.pid" runtime.type))

        (call .system.ranged_domain_type (type exec.type lowhigh))
        (call .init.named_socket_activation (type runtime.type))

        (call .seutil.libselinux_linked (type))

        (call .logging.send_syslog_messages (type))

        (call .sys.devices.read_simple_files (type))
        (call .sys.devices.cpu.list_dirs (type))
        (call .sys.devices.cpu.online.read_files (type))

        (call .kernel.proc.miscinfo.read_files (type))
        ; /proc/sys/crypto/fips_enabled
        (call .kernel.proc.sysctl.crypto.search_dirs (type))
        (call .kernel.proc.sysctl.crypto.read_files (type))

        (call .file.misc.locale.read_locales (type))

        ;;
        ;; Macros
        ;;

        (macro admin ((role role)(type domain))
            (call unit.admin (role domain))
        )
    )

    (block logd
        ;;
        ;; Declarations
        ;;

        (blockinherit .domain.template)
        (blockinherit .system.template_daemon_domain)

        (block conf
            (blockinherit .virt.conf.template)
            (blockinherit .file.template_file)
        )

        (block runtime
            (blockinherit .virt.runtime.template)
            (blockinherit .file.template_file)
            (blockinherit .file.template_sock_file)

            (filecon "/run/libvirt/virtlogd-admin-sock" socket context_lowhigh)
            (filecon "/run/libvirt/virtlogd-sock" socket context_lowhigh)
            (filecon "/run/virtlogd\.pid" file context_lowhigh)
        )

        (block unit
            (blockinherit .init.unit.template)
        )

        ;;
        ;; Contexts
        ;;

        (filecon "/usr/sbin/virtlogd" file exec.context_lowhigh)

        (filecon "/etc/default/virtlogd" file conf.context_lowhigh)
        (filecon "/etc/libvirt/virtlogd\.conf" file conf.context_lowhigh)

        (filecon "/usr/lib/systemd/virtlogd-admin\..*" any unit.context_lowhigh)
        (filecon "/usr/lib/systemd/virtlogd\..*" any unit.context_lowhigh)

        ;;
        ;; Policy
        ;;

        (allow type self (process (getsched)))
        (allow type self .unix_stream_socket.create)
        (allow type self .unix_stream_socket.listen)
        (allow type self .unix_stream_socket.accept)

        (call rw_inherited_pipes (type))

        (call .system.ranged_domain_type (type exec.type lowhigh))

        (call .virt.conf.read_files (type))
        (call conf.read_files (type))

        (call runtime.manage_files (type))
        (call runtime.manage_sock_files (type))
        (call .virt.runtime.type_transition (type sock_file "virtlogd-admin-sock" runtime.type))
        (call .virt.runtime.type_transition (type sock_file "virtlogd-sock" runtime.type))
        (call .virt.runtime.manage_dirs (type))
        (call .virt.runtime.automatic_type_transition (type))
        (call .file.runtime.type_transition (type file "virtlogd.pid" runtime.type))

        (call log.rw_dirs (type))
        (call log.create_files (type))
        (call log.read_files (type))
        (call log.append_files (type))

        ; Prevent erasure of log files
        (neverallow type log.type (file (write unlink)))

        (call .init.named_socket_activation (type runtime.type))
        (call .systemd.notify_startup (type))

        (call .seutil.libselinux_linked (type))
        (call .seutil.translate_context (type))

        (call .sys.devices.read_simple_files (type))
        (call .sys.devices.cpu.list_dirs (type))
        (call .sys.devices.cpu.online.read_files (type))

        ; /proc/sys/crypto/fips_enabled
        (call .kernel.proc.sysctl.crypto.search_dirs (type))
        (call .kernel.proc.sysctl.crypto.read_files (type))

        (call .nss.client.domain_type (type))

        (call .file.misc.locale.read_locales (type))

        (call .logging.send_syslog_messages (type))

        ; FIXME:
        (call .file.exec.search_dirs (type))

        ;;
        ;; Macros
        ;;

        (macro stream_connect ((type domain))
            (allow domain self .unix_stream_socket.create)
            (allow domain self .unix_stream_socket.connect)
            (call .virt.runtime.search_dirs (domain))
            (call runtime.write_sock_files (domain))
            (allow domain type .unix_stream_socket.connectto)
        )

        (macro client ((type domain))
            (call stream_connect (domain))
            (call .domain.read_state_pattern (type domain))
            (call use_fd (domain))
            (call rw_inherited_pipes (domain))
        )

        (macro admin ((role role)(type domain))
            (call conf.admin_files (role domain))
            (call unit.admin (role admin))
        )
    )

    (block leasehelper
        ;;
        ;; Declarations
        ;;

        (blockinherit .domain.template)
        (blockinherit .system.template_domain)

        (block runtime
            (blockinherit .file.runtime.template)
            (blockinherit .file.template_file)
        )

        ;;
        ;; Contexts
        ;;

        (filecon "/usr/lib/libvirt/libvirt_leaseshelper" file exec.context_lowhigh)

        (filecon "/run/leaseshelper\.pid" file runtime.context)

        ;;
        ;; Policy
        ;;

        (allow type self (process (getsched)))

        (call runtime.manage_files (type))
        (call .file.runtime.type_transition (type file "leaseshelper.pid" runtime.type))

        (call .seutil.libselinux_linked (type))

        (call daemon.use_fd (type))
        (call daemon.rw_inherited_pipes (type))

        (call .kernel.proc.miscinfo.read_files (type))

        (call .sys.devices.read_simple_files (type))
        (call .sys.devices.cpu.read_simple_files (type))
        (call .sys.devices.cpu.online.read_files (type))

        ; FIXME:
        (call .virt.state.search_dirs (type))

        (optional opt_dnsmasq
            (call .dnsmasq.state.rw_dirs (type))
            (call .dnsmasq.state.manage_files (type))
        )
    )

    ;;
    ;; Macros
    ;;

    (macro admin ((role role)(type domain))
        (call conf.admin_all_simple_files (role domain))
        (call state.admin_all_simple_files (role domain))
        (call daemon.admin (role domain))
        (call lockd.admin (role domain))

        ; FIXME:
        (call .svirt.image.admin_simple_files (role domain))
    )
)

(in .dnsmasq
    (filecon "/run/libvirt/network(/.*)?" any runtime.context_lowhigh)
    (filecon "/var/lib/libvirt/dnsmasq(/.*)?" any state.context_lowhigh)

    (call .virt.runtime.search_dirs (type))
    (call .virt.state.search_dirs (type))

    (call .virt.leasehelper.domtrans (type))
)

(in .dpkg
    ; FIXME:
    (call .svirt.image.admin_simple_files (roles type))
)
