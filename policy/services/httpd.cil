;;
;; httpd
;;
; Basic domain for all webservers

(block httpd
    ;;
    ;; Declarations
    ;;

    (blockinherit .domain.template)
    (blockinherit .system.template_daemon_domain)

    ;;
    ;; Contexts
    ;;

    (filecon "/usr/sbin/nginx" any exec.context_lowhigh)

    ;;
    ;; Configuration Files
    ;;
    (block conf
        ;;
        ;; Declarations
        ;;

        (blockinherit .file.conf.template)
        (blockinherit .file.template_simple_file)

        ;;
        ;; Contexts
        ;;

        (filecon "/etc/default/nginx" file context_lowhigh)
        (filecon "/etc/nginx(/.*)?" any context_lowhigh)

        ;;
        ;; Policy
        ;;

        (call .init.daemon_conf_type (type))
    )

    ;;
    ;; State Files
    ;;
    (block state
        ;;
        ;; Declarations
        ;;

        (blockinherit .file.var.lib.template)
        (blockinherit .file.template_dir)
        (blockinherit .file.template_file)

        ;;
        ;; Contexts
        ;;

        (filecon "/var/lib/nginx(/.*)?" any context)

        ;;
        ;; Policy
        ;;

        (call .init.daemon_state_type (type))
    )

    ;;
    ;; Log Files
    ;;
    (block log
        ;;
        ;; Declarations
        ;;

        (blockinherit .file.var.log.template)
        (blockinherit .file.template_dir)
        (blockinherit .file.template_file)

        ;;
        ;; Contexts
        ;;

        (filecon "/var/log/nginx(/.*)?" any log.context)

        ;;
        ;; Policy
        ;;

        (call .constraints.rbacsep.exemption_object_type (type))

        (call .init.daemon_log_type (type))

        (optional opt_logrotate
            (call .logrotate.client.log.obj_type (type))
        )
    )

    ;;
    ;; Runtime Files
    ;;
    (block runtime
        ;;
        ;; Declarations
        ;;

        (blockinherit .file.runtime.template)
        (blockinherit .file.template_file)

        ;;
        ;; Contexts
        ;;

        (filecon "/run/nginx\.pid" file runtime.context)
        (filecon "/run/nginx@[^/]+\.pid" file runtime.context)

        ;;
        ;; Policy
        ;;

        (call .init.daemon_runtime_type (type))
    )

    ;;
    ;; Loadable Modules
    ;;
    (block modules
        ;;
        ;; Declarations
        ;;

        (blockinherit .file.misc.template)
        (blockinherit .file.template_simple_file)

        ;;
        ;; Contexts
        ;;

        (filecon "/usr/share/nginx/modules-available(/.*)?" any modules.context_lowhigh)
        (filecon "/usr/share/nginx/modules(/.*)?" any modules.context_lowhigh)
        (filecon "/usr/lib/nginx/modules(/.*)?" any modules.context_lowhigh)
    )

    ;;
    ;; Systemd Units
    ;;
    (block unit
        ;;
        ;; Declarations
        ;;

        (blockinherit .init.unit.template)

        ;;
        ;; Contexts
        ;;

        (filecon "/usr/lib/systemd/system/nginx\..*" any context_lowhigh)
        (filecon "/usr/lib/systemd/system/nginx@.*" any context_lowhigh)
    )

    ;;
    ;; Web Content Files
    ;;
    (block content
        ;;
        ;; Declarations
        ;;

        (blockinherit .file.misc.template_generic)

        ;;
        ;; Templates
        ;;

        (block template
            (blockabstract template)

            ;;
            ;; Declarations
            ;;

            (blockinherit .file.misc.template)
            (blockinherit .file.template_simple_file)

            (call .httpd.content.specific_type (type))

            ;;
            ;; Policy
            ;;

            (call .constraints.rbacsep.exemption_object_type (type))
        )

        ;;
        ;; Childs
        ;;

        (block system
            (block ro
                (blockinherit template)

                (filecon "/var/www(/.*)?" any context_lowhigh)
                (filecon "/usr/share/nginx/html(/.*)?" any context_lowhigh)
            )

            (block rw
                (blockinherit template)
            )
        )
    )

    ;;
    ;; Policy
    ;;

    (allow type self (process (signal)))
    (allow type self .capability.dac_override)
    (allow type self .capability.dac_read_search)
    (allow type self .tcp_socket.listen)
    (allow type self .tcp_socket.accept)
    (allow type self .tcp_socket.shutdown)

    (call conf.list_dirs (type))
    (call conf.read_files (type))
    (call conf.read_lnk_files (type))

    (call state.manage_dirs (type))
    (call state.manage_files (type))
    (call .file.var.lib.type_transition (type dir "nginx" state.type))

    (call log.manage_dirs (type))
    (call log.manage_files (type))
    (call .file.var.log.type_transition (type dir "nginx" log.type))

    (call runtime.manage_files (type))
    (call .file.runtime.type_transition (type file "*" runtime.type))

    (call modules.search_dirs (type))
    (call modules.map_exec_files (type))
    (call modules.read_lnk_files (type))

    (call content.system.ro.list_dirs (type))
    (call content.system.ro.read_files (type))
    (call content.system.ro.read_lnk_files (type))
    (neverallow type content.system.ro.type (dir (add_name remove_name write)))
    (neverallow type content.system.ro.type (file (write append)))
    (neverallow type content.system.ro.type (lnk_file (write append)))

    (call .net.port.http.tcp_name_bind (type))
    (call .net.port.https.tcp_name_bind (type))
    (call .net.node.tcp_node_bind (type))
    ; ocsp check
    (call .net.port.http.tcp_connect (type))

    (call .nss.client.domain_type (type))

    (call .kernel.proc.miscinfo.read_files (type))

    ; /sys/devices/system/cpu/online
    (call .sys.devices.cpu.online.read_files (type))

    ; /proc/sys/kernel/ngroups_max
    (call .kernel.sysctl.search_dirs (type))
    (call .kernel.sysctl.read_files (type))

    (call .file.cert.conf.read_files (type))

    (call .file.misc.locale.read_locales (type))

    (optional opt_php
        (optional opt_php_fpm
            (call php.fpm.unix_stream_connect (type))
        )
    )

    ;;
    ;; Macros
    ;;

    (macro admin ((role role)(type domain))
        (call unit.admin (role domain))
        (call conf.admin_simple_files (role domain))
        (call state.admin_dirs (role domain))
        (call state.admin_files (role domain))
        (call log.admin_dirs (role domain))
        (call log.admin_files (role domain))
        (call modules.read_simple_files (domain))
        (call content.system.ro.admin_simple_files (role domain))
        (call content.system.rw.admin_simple_files (role domain))
    )
)

; nginx-common.postinst workaround
(in .dpkg.script
    (rangetransition type .httpd.log.type file .lowlow)

    ; nginx-common.postinst: runs mkdir -p /var/www
    (call .file.var.type_transition (type dir "www" .httpd.content.system.ro.type))
)

; start-stop-daemon helper in system unit
(in .init.rc
    (call .httpd.runtime.read_files (type))
    (call .httpd.signal (type))
    (call .httpd.signull (type))
)
