# Constraints

This directory contains the modules that implement the constraints necessary for MCS, IBAC, RBAC and RBACSEP.

Implemented and verified to be correct:

- [x] IBAC
- [x] RBAC
- [ ] MCS
- [ ] RBACSEP

Permissions are generally split into `identity_change`, `read`, `write` and a few others, see [../../base/permission_sets.cil](../../base/permission_sets.cil)

## MCS

Ensure that a domain can only access contexts it dominates.

All domains are restricted by default.

Communication between two domains is allowed if the clients clearance dominates the servers current.
This means that a client with `s0:c1.c2-s0:c1.c2` can connect to a server labeled `s0-s0` or one labeled `s0:c1-s0:c1` but not if it is labeled `s0:c3-s0:c1.c3`.
The same restrictions apply to inherited file descriptors, connected sockets, ... .

When a domain with a higher clearance connects (over a unix socket) to a domain with lower clearance, the "connection socket" gets the higher clearance.
This means that things like `dbus-daemon` that do their own acv checks need to run as `high` to allow connections from every clearance and need seperate constraints.

### TODO

* Figure out if lifting the `(eq l2 h2)` restrictions for files can be used for something good, e.g. allow a domain to read an object if `(dom h1 l2)` but only write if `(dom h1 h2)`
    See https://lore.kernel.org/selinux/20091103114530.GH1672@myhost.felk.cvut.cz/ and https://lore.kernel.org/selinux/20091125202727.GD1649@myhost.felk.cvut.cz/ for prior art.

## IBAC

Prevent changes to the user part of a context.

## RBAC

Prevent changes to the role part of a context.

## RBACSEP

Prevent access between contexts with different roles.
