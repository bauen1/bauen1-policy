# base

This is a core model that should **never** be removed.
It contains declarations for classes and access vectors, mcs categories and policy capabilities.
