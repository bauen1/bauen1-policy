# kernel

These modules establishe all basic types and define all initial security identifier contexts.

Most modules in this group only make minimal assumptions about the filesystem layout.

**NOTE:** these modules still depend on `system.user` and `system.role` from the [system.cil module](../system/system.cil) and a few templates
