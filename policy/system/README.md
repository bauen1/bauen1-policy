# system

This group contains all modules necessary for the core functionality of a debian system.

* [systemd](./systemd) contains almost all modules specific to systemd, notable exceptions are [udev.cil](./udev.cil), [logging.cil](./logging.cil) and [init](./init.cil)
* [file](./file)
