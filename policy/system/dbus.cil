;;
;; D-BUS
;;
; Overloads dbus in policy/base/permission_sets.cil

(in .dbus
    ;;
    ;; Configuration Files
    ;;
    (block conf
        ;;
        ;; Declarations
        ;;

        (blockinherit .file.conf.template)
        (blockinherit .file.template_dir)
        (blockinherit .file.template_file)

        ;;
        ;; Contexts
        ;;

        (filecon "/etc/dbus-1(/.*)?" any context_lowhigh)
        (filecon "/etc/default/dbus" any context_lowhigh)

        ;;
        ;; Policy
        ;;

        (call .init.daemon_env_file_type (type))
    )
)

; common things shared between system and session dbus
(in .dbus
    ;;
    ;; Declarations
    ;;

    (blockinherit .utils.generic.template)

    ;;
    ;; Neverallows
    ;;

    (neverallow all_types not_types .dbus.acquire_svc)

    ;;
    ;; Policy
    ;;

    (allow types self .process.signal)
    (allow types self .process.sigkill)
    (allow types self .unix_stream_socket.create)
    (allow types self .unix_stream_socket.bind)
    (allow types self .unix_stream_socket.accept)
    (allow types self .unix_stream_socket.shutdown)
    ; dropping privileges
    (allow types self .process.getcap)

    (call conf.list_dirs (types))
    (call conf.read_files (types))
    (call conf.watch_dirs (types))

    (call .dev.random.read_chr_files (types))

    (call .file.misc.locale.read_locales (types))
    ; /usr/share/dbus-1/
    (call .file.usr.read_simple_files (types))
    (call .file.usr.watch_dirs (types))

    ; check if apparmor is enabled
    ; /sys/module/apparmor/parameters/enabled
    (call .sys.module.apparmor.search_dirs (types))
    (call .sys.module.apparmor.read_files (types))
    ;
    (call .sys.kernel.search_dirs (types))

    ; selinux integration
    (call .seutil.setfscreate (types))
    (call .seutil.translate_context (types))
    (call .selinux.compute_access_vector (types))
    (call .logging.send_syslog_messages (types))
    (call .logging.send_audit_messages (types))

    (call .kernel.proc.getattr_fs (types))
    ; /proc/sys/kernel/random/boot_id
    (call .kernel.sysctl.search_dirs (types))
    (call .kernel.sysctl.read_files (types))

    (call .systemd.logind.client.session.domain_type (types))
    (call .systemd.logind.client.inhibit.domain_type (types))

    (call .nss.client.domain_type (types))

    (call .fs.security.search_dirs (types))
    (call .fs.cgroup.getattr_fs (types))
    (call .fs.cgroup.search_dirs (types))

    ;;
    ;; Templates
    ;;

    (block template
        (blockabstract template)

        ;;
        ;; Declarations
        ;;

        (call .dbus.specific_type (type))

        ;;
        ;; Policy
        ;;

        (call rw_inherited_pipes (type))
    )
)

; system dbus
(in .dbus
    ;;
    ;; Declarations
    ;;

    (blockinherit .domain.template)
    (blockinherit .system.template_daemon_domain)

    (blockinherit .dbus.template)

    ;;
    ;; State Files
    ;;
    (block state
        ;;
        ;; Declarations
        ;;

        (blockinherit .file.var.lib.template)
        (blockinherit .file.template_simple_file)

        ;;
        ;; Contexts
        ;;

        (filecon "/var/lib/dbus(/.*)?" any context_lowhigh)
    )

    ;;
    ;; Runtime Files
    ;;
    (block runtime
        ;;
        ;; Declarations
        ;;

        (blockinherit .file.runtime.template)
        (blockinherit .file.template_dir)
        (blockinherit .file.template_sock_file)

        ;;
        ;; Contexts
        ;;

        (filecon "/run/dbus(/.*)?" any context_lowhigh)

        ;;
        ;; Policy
        ;;

        (call .constraints.rbacsep.exemption_object_type (type))
    )

    ;;
    ;; Systemd Units
    ;;
    (block unit
        ;;
        ;; Declarations
        ;;

        (blockinherit .init.unit.template)

        ;;
        ;; Contexts
        ;;

        (filecon "/usr/lib/systemd/system/dbus\..*" any context_lowhigh)
    )

    ;;
    ;; Contexts
    ;;

    (filecon "/usr/bin/dbus-daemon" any exec.context_lowhigh)

    ;;
    ;; Policy
    ;;

    ; dropping privileges
    (allow type self (capability (setuid setgid)))
    (allow type self .process.setcap)

    ; We have our own mcs constraints and clients from all clearances
    (call .system.ranged_domain_type (type exec.type lowhigh))

    (call .constraints.rbacsep.exemption_domain_type (type))
    (call .constraints.rbacsep.domain_connectto_unconditional_exemption_target_type (type))
    (call .constraints.rbacsep.domain_send_msg_unconditional_exemption_source_type (type))
    (call .constraints.rbacsep.domain_send_msg_unconditional_exemption_target_type (type))

    (call .init.named_socket_activation (type runtime.type))

    (call .systemd.notify_startup (type))

    (call state.search_dirs (type))
    (call state.read_files (type))

    ;;
    ;; Macros
    ;;

    (macro admin ((role role)(type domain))
        (call conf.admin_dirs (role domain))
        (call conf.admin_files (role domain))
        (call state.admin_dirs (role domain))
        (call state.admin_files (role domain))

        (call unit.admin (role domain))
    )

    ;;
    ;; Clients
    ;;

    (block client
        ;;
        ;; Declarations
        ;;

        (blockinherit .utils.generic.template)

        ;;
        ;; Policy
        ;;

        (allow types self .unix_stream_socket.create)
        (allow types self .unix_stream_socket.bind)
        (allow types self .unix_stream_socket.connect)
        (call runtime.search_dirs (types))
        (call runtime.rw_sock_files (types))
        (allow types .dbus.type .unix_stream_socket.connectto)
        (allow types .dbus.type .dbus.send_msg)

        (call .domain.read_state_pattern (.dbus.type types))
        (call .dbus.read_state (types))

        (allow types self .dbus.send_msg)
        (allow types .dbus.type .dbus.send_msg)
        (allow .dbus.type types .dbus.send_msg)

        (macro domain_type ((type domain))
            (call specific_type (domain))
        )

        (macro acquire_service ((type domain))
            (allow domain .dbus.type .dbus.acquire_svc)
        )
    )
)

; session dbus
(in .dbus
    (block session
        ;;
        ;; Declarations
        ;;

        (blockinherit .utils.generic.template)

        ;;
        ;; User Runtime Files
        ;;
        (block user_runtime
            ;;
            ;; Declarations
            ;;

            (blockinherit .user.runtime.template)
            (blockinherit .file.template_dir)
            (blockinherit .file.template_sock_file)

            ;;
            ;; Contexts
            ;;

            (filecon "/run/user/%{USERID}/bus" socket context_lowhigh)
            (filecon "/run/user/%{USERID}/dbus-1" dir context_lowhigh)
            (filecon "/run/user/%{USERID}/dbus-1/.*" dir context_lowhigh)
        )

        ;;
        ;; Policy
        ;;

        (call user_runtime.manage_dirs (types))
        (call user_runtime.manage_sock_files (types))
        (call .user.runtime.type_transition (types dir "dbus-1" user_runtime.type))
        (call .user.runtime.type_transition (types sock_file "bus" user_runtime.type))

        (call .file.exec.search_dirs (types))

        (call .systemd.user.named_socket_activation (types .dbus.session.user_runtime.type))
        (call .systemd.user.notify_startup (types))

        ;;
        ;; Templates
        ;;

        (block template
            (blockabstract template)

            ;;
            ;; Declarations
            ;;

            (blockinherit .application.template_domain)
            (blockinherit .dbus.template)

            (call domain_entry_file (.dbus.exec.type))

            (call .dbus.session.specific_type (type))

            (roletype roles .dbus.session.user_runtime.type)

            ;;
            ;; Macros
            ;;

            (macro run ((role role)(type domain))
                (call run_pattern (role domain .dbus.exec.type))
            )

            ;;
            ;; Clients
            ;;

            (block client
                ;;
                ;; Declarations
                ;;

                (blockinherit .utils.generic.template)

                (call specific_type (.dbus.session.client.types))

                ;;
                ;; Policy
                ;;

                (allow types self .unix_stream_socket.create)
                (allow types self .unix_stream_socket.connect)
                (call .dbus.session.user_runtime.search_dirs (types))
                (call .dbus.session.user_runtime.write_sock_files (types))
                (allow types type .unix_stream_socket.connectto)

                (call .domain.read_state_pattern (type types))

                (allow types self .dbus.send_msg)
                (allow types type .dbus.send_msg)
                (allow type types .dbus.send_msg)

                (macro domain_type ((type domain))
                    (call specific_type (domain))
                )

                (macro acquire_service ((type domain))
                    (allow domain type .dbus.acquire_svc)
                )
            )
        )

        (block template_user
            (blockabstract template_user)

            (block dbus
                (blockinherit .dbus.session.template)

                (optional opt_systemd_user
                    (call systemd.daemon_type (type .dbus.exec.type))
                )
            )

            (call automatic_feedback_transition_pattern (dbus.type .file.exec.type))
            (call automatic_feedback_transition_pattern (dbus.type .file.exec.shell.type))
            (call domain_entry_file (.file.exec.type))
            (call domain_entry_file (.file.exec.shell.type))
        )

        ;;
        ;; Clients
        ;;

        (block client
            ;;
            ;; Declarations
            ;;

            (blockinherit .utils.generic.template)

            ;;
            ;; Macros
            ;;

            (macro domain_type ((type domain))
                (call specific_type (domain))
            )

            (macro acquire_service ((type domain))
                (allow domain .dbus.session.types .dbus.acquire_svc)
            )
        )
    )
)

(in .systemd.tmpfiles
    ; /usr/lib/tmpfiles.d/dbus.conf

    ; d /var/lib/dbus 0755 - - -
    (call .dbus.state.manage_dirs (type))
    (call .dbus.state.relabel_dirs (type))

    ; L /var/lib/dbus/machine-id - - - - /etc/machine-id
    (call .dbus.state.getattr_files (type))
    (call .dbus.state.manage_lnk_files (type))
    (call .dbus.state.relabel_lnk_files (type))

    ; d /run/dbus/containers 0755 messagebus - - -
    (call .dbus.runtime.manage_dirs (type))
    (call .dbus.runtime.relabel_dirs (type))
)

; systemd reload script
(in .init.rc
    (call .dbus.client.domain_type (type))
)
