# bauen1-policy

![](./docs/selinux-penguin.svg)

A policy in cil for personal use that supports a modern debian (11+) system with systemd.

## Features

* Fine grained ioctl permissions, preventing e.g. [SELinux sandbox escape via TIOCSTI ioctl](https://lore.kernel.org/selinux/20160923112350.89325hv7y899lack@webmail.alunos.dcc.fc.up.pt/)
* PostgreSQL SELinux module (sepgsql), see [docs/sepgsql.md](docs/sepgsql.md)
* Multi-category Security, see [docs/MCS.md](docs/MCS.md)

## Requirements

Debian unstable, for details see [./debian/control](./debian/control).

Most importanly, currently only `systemd-networkd` or `NetworkManager` are supported for configuring the network.

The root filesystem must be `ext4`.

## Build and Install

It is recommended to boot with selinux disabeled when performing these steps.

1. Install the new policy

    ```sh
    make validate
    sudo make install
    ```

2. Enable the policy by editing `/etc/selinux/config` and changin `SELINUXTYPE=...` to `SELINUXTYPE=bauen1-policy`

3. Fix the labels before the first reboot, otherwise systemd will fail to bring up important services / sockets.

    ```sh
    sudo setfiles /etc/selinux/bauen1-policy/contexts/files/file_contexts /
    # Until an option for bind-mount relabeling using fixfiles is merged upstream this works around some issues
    sudo mount --bind / /mnt
    sudo setfiles -r /mnt /etc/selinux/bauen1-policy/contexts/files/file_contexts /mnt/
    sudo umount /mnt
    ```

    If the installed `fixfiles` script supports the `-M` option, this can be reduced to:

    ```sh
    sudo fixfiles -F -M /
    ```

4. Setup login mappings using ***semanage(8)***

    You can skip this if your system administrators have been added to the `sudo` group, the policy already contains mappings for that.

    For example to have a user named `sysadmin` login with a staff selinux user (combination of regular user and admin user):
    ```sh
    sudo semanage login --add -s staff.user -r s0-s0:c0.c15 sysadmin
    ```

5. Enable selinux and fix labels on next boot and reboot.

    ```sh
    sudo selinux-activate
    sudo fixfiles -F onboot
    sudo reboot
    ```

6. Optional: edit `/etc/sudoers` and add `ROLE=sysadm.role` for your sysadmin sudoer entries for using sudo as usual.

7. After confirming that everything is working as expected, reboot into enforcing mode.

## Assumptions

* Every type is part of `domain.types` or `file.types` or similiar.
    In practice that means a type should never be declared manually and instead an appropiate template should be used.
* Symlinks to an executable (aliases) should always have the same label, e.g. `/usr/sbin/vgs` is a symlink to `/usr/sbin/lvm`, they should both be labeled with `system.user:system.role:lvm.exec.type:s0-s0.c0-c15`

## Wishlist for CIL

* Using `in` to enter inherited namespaces
* Anonymous typeattributesets for neverallows

        (typeattribute not_kernel_types)
        (typeattributeset not_kernel_types (not kernel.type))
        (neverallow not_kernel_types self (process (setcurrent)))

    Would become

        (neverallow (not kernel.type) self (process (setcurrent)))

    This avoids repetition and makes a lot of things easier to read

* Allow extended permission sets (`permissionx`) to be composed of multiple already defined extended permission sets
* Allow extended permission sets to be part of a `classpermission`

## Notes

* Use `useradd`, `userdel` instead of the debian `adduser`, `deluser`
* It is suggested to enable `expand-check` in `semodule.conf`

## Postfix

Postfix is support but only without chroot.

Debian ships with chroot enabled by default, running `postfix-nochroot` and `echo 'SYNC_CHROOT=""' > /etc/default/postfix && restorecon -vF /etc/default/postfix` is necessary to disable it.
After restarting the `postfix@-` service you can cleanup the left over files `rm -rf /var/spool/postfix/usr /var/spool/postfix/etc /var/spool/postfix/dev /var/spool/postfix/lib`

`postfix-nochroot` doesn't work, manually change `/etc/postfix/master.cf`

## lightdm

Disable lightdm `systemd --user` instance:

```bash
sudo systemctl mask user@"$(id -u lightdm)"
```

## License

Copyright (c) 2019-2022 Jonathan Hettwer (bauen1) See [LICENSE](LICENSE)

Logo designed by [*Máirín Duffy*](http://pookstar.deviantart.com/) (CC BY-SA 2.5)
