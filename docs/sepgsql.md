# SELinux support for PostgreSQL (sepgsql)

Currently MCS and TE is supported, but IBAC and RBAC are lacking or completely missing.

1. Install postgresql
2. Enable the `sepgsql` module
3. Fix labels and enable selinux for a database by running `sudo -u postgres psql -f /usr/share/postgresql/12/contrib/sepgsql.sql`

An example database can be prepared using [sepgsql.sql](./sepgsql.sql)
