# style

This document describes the style that all modules should follow.

The biggest exception to this are the modules part of the [base](policy/base/) group.

## File Contexts

* Files should usually be labeled with the range `low-high` or `context_lowhigh`, this is especially important for executables.

    This ensures that a process with less than `high` clearance can't modify a program later run by a process with more privileges, regardless of type enforcement rules.

## Order

* dir
* lnk_file
* file
* fifo_file
* sock_file
* chr_file
* blk_file

## Example

```secil
;;
;; <module_name>: <short summary>
;;
; A longer description and links to various references can follow
; It is entirely optional

(block <module_name>
    ;;
    ;; Declarations
    ;;


    ;;
    ;; Templates
    ;;

    (block template
        (blockabstract template)

        ;;
        ;; Declarations
        ;;

        (type type)
        (call <module_name>.specific_type (type))

        ;;
        ;; Policy
        ;;

        ;;
        ;; Macros
        ;;
    )
)

; Any in statements used to modify, e.g. `.init.rc` should follow after the main block(s)
(in .init.rc
    (allow type self (capability (dac_override)))
)
```

## More things to consider

- Always prefer calling macros instead of using `allow`
    For example use `(call myfile.getattr_files (type))` instead of `(allow type myfile.type (file (getattr)))`.
    If you use `allow` you might be responsible for additional `allowx` statements to deal with ioctl whitelists

    This involves some "funny" things like:
    ```secilc
    (block mydomain
        (call rw_inherited_pipes (type))
    )
    ```
    instead of
    ```secilc
    (block mydomain
        (allow type self fifo_file_perms.rw_inherited)
    )
    ```
- When refering to a global, always do so explicitly using a leading `.`
