# IPSec Support Example

This example assumes two host named `sel1` and `sel2`.

## MCS Categories

* dn42
* lab
* private
* home


4 Nodes

All interconncted with labeled IPSec, TPM + SecureBoot, DNSSEC enabled resolver

## Home

## Lab

## vps0

## vps1


# Setup for each VM

1. Install libreswan > 3.32

2. Start and enable `ipsec.service`

3. Add the connection config on both hosts:

    ```
    # /etc/ipsec.d/tunnel1.conf
    conn tunnel1
        left=sel1.tld
        right=sel2.tld
        authby=secret
        labeled-ipsec=yes # probably not necessary
        policy-label=system.user:system.role:ipsec_test.type:s0
        ikev2=never # important, labeled ipsec is only supported for ikev1
    ```

4. Add the secret on both hosts, e.g.:

    ````
    # /etc/ipsec.d/tunnel1.secrets
    sel1.tld sel2.tld : PSK "mysecretpsk"
    ````

5. Load the tunnel using `sudo ipsec auto --add tunnel1`

6. Start the tunnel using `sudo ipsec auto --up tunnel1`

## Example using getpeercon

Source: https://securityblog.org/2007/05/28/secure-networking-with-selinux/
