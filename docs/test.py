#!/usr/bin/env python3
# This script validates some assumptions made in the policy
# that can't really be verified using cil

import setools
import argparse

parser = argparse.ArgumentParser(description = "policy unit tests")
parser.add_argument("policy", help = "Path to the SELinux policy to query")

args = parser.parse_args()

p = setools.SELinuxPolicy(args.policy)

def system_exclusive_types():

    # Fail if the attribute doesn't exist
    r = 1

    for attr in setools.TypeAttributeQuery(p, name = "system.exclusive_types").results():
        if r == 1:
            r = 0

        for t in attr.expand():
            # print("3")
            for r in setools.RoleQuery(p, types = [ t ]).results():
                if r != "system.role":
                    print("Failure: {} is allowed {}".format(r, t))
                    r = 2

    return r

print("Testing if only system.role can associate with system.exclusive_types:")
r = system_exclusive_types()
if r == 0:
    print("Success")
elif r == 1:
    print("Missing attribute")
else:
    print("Failure")

print("Testing that no role is allowed to transition to system.role: ", end='')

for role_allow in p.rbacrules():
    assert(role_allow.target != "system.role")

print("Success")

print("Testing every type is a part of domain.types or file.types: ", end='')

print("Success")

print("All tests passed.")
