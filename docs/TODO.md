# TODO

- [x] Add new classes.
- [x] Update [[../README.md]] with merged `-M` option of `restorecon`
- [ ] Rework device neverallowxs to specify a list of allowed ioctls for a device, and prevent any other ioctl
- [ ] Add global neverallowx rule to prevent accidential broad ioctl access
- [x] Review linux 5.16 changes
  * New policy changes
  * New policy classes (`mctp_socket`, `io_uring`)

- [ ] Add a script to build and run a test VM and collect the denials from boot -> login -> shutdown
 - [x] Automate debian installer, done in `lab/iac`
 - [ ] Write a testsuite, e.g.:
  - Install podman
  - Pull `hello-world` image
  - Run and verify output
 - [ ] Extract syslog, audit logs from VM image

- [ ] Find a nice way to ship all optional modules as installed but disabled or put them in a different package

- [ ] Make use of `in after`, `in before` added recently
- [ ] Improve consistency in naming things
  * Rename `file.var.lib` to `file.state`
  * Rename `file.var.log` to `file.log`
  * Fix auditd, syslogd, journald, ausearch, etc. domain names.

- [ ] Write a script to verify that filecons generally adhere to the correct MLS range, and that the type is part of `.file.<class>_types`

- [ ] Label `/etc/shadow` high-high ?

- [ ] Figure out a good format for comments, e.g. doxygen. from which documentation about the macros can be generated in, e.g. similar to the HTML for the CIL Reference Manual

- [ ] Implement basic unit tests and more neverallows
  * Ensure that every type is part of either `domain.types`, `file.types`, `port.types`, `node.types`, ...
  * Ensure that unprivileged roles can never change to an admin domain, e.g. `seutil.semodule.type`
  * Implement something like `neverroleallow` to ensure, e.g. that a netif type can only be associated with `system.role`

- [ ] Add additional types for very common sysctls, e.g. `cap_last_cap`, `fips_enabled`. Most (read) users of kernel sysctls only need these 2 but nothing else, this would tighten up access quite a bit.
    * systemd probably only needs to write 3-5 too.
- [ ] Allow opting out of ioctl extended permissions by putting it behind a tunable (perhaps per class i.e. seperate files, chr/blk, sockets)
- [ ] Improve `.mount.libmount_list_mounts` macro to include access to `/etc/mtab` or maybe fix the label of `/etc/mtab`.
- [ ] Improve `/etc/machine-id`, and the `/var/lib/dbus/machine-id` file, and macros to access it, e.g. related to dbus clients ?
- [ ] Find reason for new ioctl (but ioctlcmd=0x0) denials with podman / linux 6.0.0, review dbus ioctl permissions (fd passing requires the normal ioctl access to the file, but not any specific ioctlcmd)

### constraints

- [ ] Write a textual description of the constraints
  - [ ] IBAC: Probably the easiest: Ensure that a user can only create new objects / subjects with his own user
  - [ ] RBAC: Same
  - [ ] RBACSEP: Will be complicated
- [ ] Add an almost complete obt-out macro to `policy/constraints/constraints.cil`, similar to `unconfined` in refpolicy
- [ ] Verify that IBAC constraints match what they're supposed to do.

- [ ] Completely implement constraints for userspace classes (`dbus`, `service`)
- [ ] ensure high integrity for e.g. module loads, e.g. `(system (module_request))` and e.g. setting the clock `(capability (sys_time))`
- [ ] Ensure that `/boot/efi` has a MCS range of `high-high`
- [ ] Configure number of categories at build time and generate the CIL module.
- [ ] unix socket connections get the range of the client, this means that certain constraints could fail, when they shouldn't (see https://lore.kernel.org/selinux/1205937798.6466.223.camel@moss-spartans.epoch.ncsc.mil/)
- [ ] Figure out if there is a way to upgrade a policy (adding new categories), relabel and reboot without breaking everything.

### misc

- [ ] Review all existing modules to bring them up to standard
- [ ] Better handling of inherited sockets form `systemd` or `systemd --user`
- [ ] Move the `(unix_stream_socket (shutdown))` permission back into `unix_stream_socket.create`
- [ ] Move the `(netlink_.*_socket (bind))` permission back into `netlink_.*_socket.create`
- [ ] Review the range that daemons run as
- [ ] https://github.com/systemd/systemd/pull/17169 was merged into systemd v247, remove the workaround once all systems are upgraded
- [ ] Add stub support for `/run/systemd/io.system.ManagedOOM`
- [x] Add support for systemd-suspend / systemd-sleep
- [ ] cleanup sysadm it should have at least read access to almost everywhere
- [ ] Lockdown access to memory `/dev/mem`, `/proc/kcore`, `/proc/ioports`, etc...
- [ ] Consistent Naming in `policy/kernel/fs.cil`
- [ ] Document lightdm / xfce4 setup required workarounds
- [ ] Cleanup `.user`
- [ ] Make constraints optional
- [ ] Implement a semi-unconfined module (e.g. rw all files except /usr, ...)
- [ ] Use validatetrans constraints to ensure only trusted (e.g. systemd, `user.type`) domains can elevate the integrity of e.g. files
- [ ] Maybe use validatetrans to ensure certain types can only be relabeled to / relabeled to themselfs ? (e.g. dev.storage)
- [ ] https://lore.kernel.org/selinux/52E68747.5020006@tycho.nsa.gov/
- [ ] Implement support for systemd running `sulogin` in cases of failure
- [ ] /etc/xdg/xfce4/xinitrc does mkdir on xdg directories without fixing context
- [ ] Look through <https://github.com/systemd/systemd/releases/tag/v252> and IRC logs about new monitor socket

# Development setup

```bash
sudo apt-get install --no-install-recommends -qy build-essential git debhelper devscripts
git clone --branch=dev2 "https://gitlab.com/bauen1/bauen1-policy.git"
cd bauen1-policy
git fetch origin && git reset --keep origin/dev2 && dpkg-buildpackage -b -nc && sudo debi

# testing podman
sudo apt-get install --no-install-recommends -qy podman podman-dbgsym uidmap
sudo podman image pull hello-world
sudo semodule --install local.cil && sudo podman run --rm -it --network=none hello-world
# setup .config/containers (copy from glados)
podman pull hello-world
podman run --network=none --rm -it hello-world
```

Install a `local.cil` module and diff the changes against the previous policy
```bash
sudo cp /etc/selinux/bauen1-policy/policy/policy.33 /tmp/policy.33.old && sudo semodule --install local.cil && sudo sediff /tmp/policy.33.old /etc/selinux/bauen1-policy/policy/policy.33
```

## linux perf

Reference: <https://www.paul-moore.com/blog/d/2020/12/linux_v510.html>

1. Install additional policy:

```secilc
(allow .sysadm.type self .capability2.bpf) ; perf
(allow .sysadm.type self .capability2.perfmon) ; perf
(allow .sysadm.type self (perf_event (all))) ; perf
(allow .sysadm.type self (bpf (all))) ; perf
(call .kernel.proc.kallsyms.read_files (.sysadm.type)) ; perf
(call .kernel.proc.modules.read_files (.sysadm.type)) ; perf
(call .kmod.misc.map_read_files (.sysadm.type)) ; perf

(allow .sysadm.type self .capability.sys_rawio) ; perf
(call .kernel.proc.kcore.read_files (.sysadm.type)) ; perf

; perf
(allow .sysadm.type .init.type (bpf (prog_run)))
(call .constraints.ibac.domain_self_unconditional_exemption_source_type (.sysadm.type))
(call .constraints.rbac.unconditional_exemption_source_type (.sysadm.type))

(call .fs.trace.search_dirs (.sysadm.type)) ; perf
(call .fs.trace.read_files (.sysadm.type)) ; perf

(call .fs.trace.list_dirs (.sysadm.type))
(in .fs.trace (blockinherit .file.template_file))

(allow .sysadm.type .kernel.module.type (file (link write))) ; perf
(call .fs.ns.read_files (.sysadm.type)) ; perf

; lsblk
(call .dev.loop_control.getattr_chr_files (.user.types))

; losetup
(call .dev.loop_control.rw_chr_files (.sysadm.type))
(call .dev.loop_control.ioctl_chr_files (.sysadm.type))
(allowx .sysadm.type .dev.loop_control.type (ioctl chr_file (0x4C80)))
(call .dev.storage.fixed.rw_blk_files (.sysadm.type))
(call .dev.storage.fixed.ioctl_blk_files (.sysadm.type))
(allowx .sysadm.type .dev.storage.fixed.type (ioctl blk_file (0x4C01 0x4C0A)))
(allow .kernel.type .sysadm.type .fd.use) ; losetup fd passed to kernel
(call .constraints.rbacsep.unconditional_exemption_source_type (.kernel.type)) ; ^ for (fd (use))
(call .user.home.rw_files (.kernel.type)) ; FIXME: drop
```

2. Install `linux-perf` package
3. `sudo perf record -e avc:selinux_audited -g -a`
4. `sudo perf report -g`

```bash
sudo apt-get install debian-goodies --no-install-recommends libipc-system-simple-perl libfile-which-perl libfile-slurper-perl elfutils dctrl-tools
```


`/etc/apt/sources.list`:
```
deb https://deb.debian.org/debian/ unstable main
deb http://debug.mirrors.debian.org/debian-debug/ unstable-debug main
```

`/etc/apt/preferences.d/test.pref`:
```
Package: *
Pin: origin "deb.debian.org"
Pin-Priority: 1

Package: *
Pin: origin "debug.mirrors.debian.org"
Pin-Priority: 1
```

```bash
sudo apt-get install coreutils-dbgsym
sudo sh -c 'echo 1 > /proc/sys/kernel/perf_event_paranoid'
```

use `perf trace -e avc:selinux_audited --call-graph fp`.

<https://stackoverflow.com/questions/73403115/linux-perf-not-resolving-some-symbols-with-high-addresses-starting-with-0xffffff>

Use `find-dbgsym-packages` to install additional debug symbol packages.
